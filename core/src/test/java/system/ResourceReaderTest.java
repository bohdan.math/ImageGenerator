package system;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ResourceReaderTest {

    private ResourceReader resourceReader;

    @BeforeEach
    public void setUp() {
        this.resourceReader = new ResourceReader();
    }

    @Test
    public void readAllCorrect() {
        String resourceDir = "images";

        Resource resource = resourceReader.readAllIn(resourceDir);

        assertNotNull(resource);
    }

    @Test
    public void readAllWitNullResourceDir() {
        RuntimeException exception = assertThrows(
                RuntimeException.class,
                () -> resourceReader.readAllIn(null)
        );
        assertTrue(exception.getMessage()
                .contains("Path to directory COULD NOT be null !!!"));
    }

    @Test
    public void readAllWithNoDir() {
        String resourceFile = "images/testable/4x4.jpg";
        RuntimeException exception = assertThrows(
                RuntimeException.class,
                () -> resourceReader.readAllIn(resourceFile)
        );
        assertTrue(exception.getMessage()
                .contains("Path MUST BE directory !!!"));
    }
}